### Simple client for Partisia NFT WebSocket

> to install

```sh
npm i gitlab:partisiaapplications/ethereum-nft-client
```

```js
// useage like this
import { SubscribeNft } from 'ethereum-nft-client';
onMounted(() => {
  // const url = 'ws://127.0.0.1:34534'
  const url = 'ws://partisiaauth.com/nftsocket';
  const onMint = (mint) => {
    // the minted NFT object will be called back here
    console.log('mint', mint);
  };
  SubscribeNft(url, onMint);
})
```

> onMint results in this format

```js
{
  address: '0x602606740103614fab9ec4baa0224f886afc36d0',
  nft_idx: 293,
  token_id: 1
}
```

### paths to img and json

Ex JSON: [https://partisiaauth.com/nft-json/1](https://partisiaauth.com/nft-json/1)

JSON metadata format:

```json
{
  "image": "https://partisiaauth.com/nft-img/413.png",
  "attributes": [
    {
      "trait_type": "Colours",
      "value": "Gold & Blue"
    },
    {
      "trait_type": "Inner Geometrics",
      "value": "Hex Inside Hex"
    },
    {
      "trait_type": "Zodiaks",
      "value": "None"
    },
    {
      "trait_type": "Chakras",
      "value": "None"
    },
    {
      "trait_type": "Animation",
      "value": "Static"
    }
  ]
}
```
