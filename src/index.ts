import { INftMinted, SubscribeNftClass } from './socket/subscribe';

export const SubscribeNft = (url: string, onMint: Function) => {
  return new SubscribeNftClass(url, onMint);
};
// (() => {
//   const url = 'ws://127.0.0.1:34534';
//   // const url = 'ws://partisiaauth.com/nftsocket';
//   const onMint = (mint: INftMinted) => {
//     // the minted NFT object will be called back here
//     console.log('mint', mint);
//   };
//   const subscribeNft = SubscribeNft(url, onMint);
// })();
