// for nodejs
// import w3cwebsocket from 'ws';
import { serialize, deserialize } from 'bson';

export interface INftMinted {
  address: string;
  nft_idx: number;
  token_id: number;
}
export class SubscribeNftClass {
  constructor(url: string, onMint: Function) {
    const ws = new WebSocket(url);
    // const ws = new w3cwebsocket('ws://127.0.0.1:34534');
    // const ws = new w3cwebsocket('ws://partisiaauth.com/websocket');
    // const ws = new w3cwebsocket('wss://partisiaauth.com/websocket');
    ws.binaryType = 'arraybuffer';
    // ws.on(`ping`, () => {
    //   console.log(`ping`);
    // });

    // ws.on(`pong`, () => {
    //   console.log(`pong`);
    // });

    ws.onclose = (event) => {
      console.log('close', { code: event.code, reason: event.reason });
    };
    ws.onopen = () => {
      console.log('open ws');
    };

    ws.onmessage = (ev) => {
      if (typeof ev.data === 'object') {
        // this is mint event
        const data = <ArrayBuffer>ev.data;
        const obj = <INftMinted>deserialize(data);
        // console.log(obj);
        onMint(obj);
      } else if (typeof ev.data === 'string') {
        // This is an error
        console.log(ev.data);
      }
    };
  }
}
